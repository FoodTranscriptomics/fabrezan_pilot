# Libraries ---------------------------------------------------------------
pacman::p_load(dplyr,tidyr, purrr, broom, furrr, fdrtool, densityClust, parallel, multidplyr)



# Funs --------------------------------------------------------------------
stats_fun_depth <- function(x){
lm(value_drift_corr ~ sample_point, data = x) %>% 
    anova %>%
    tidy %>% 
    filter(term == "sample_point") %>% 
    pull(p.value)
}


stats_fun_depth_norm <- function(x){
    
x <- x %>% 
        group_by(time_clust) %>% 
        mutate(value_drift_corr_norm  = value_drift_corr/mean(value_drift_corr, na.rm = TRUE)) %>% 
        ungroup
    
    
lm(value_drift_corr_norm ~ sample_point, data = x) %>% 
    anova %>%
    tidy %>% 
    filter(term == "sample_point") %>% 
    pull(p.value)
}


stats_fun_time <- function(x){
lm(value_drift_corr ~ Time_avg, data = x) %>% 
    anova %>%
    tidy %>% 
    filter(term == "Time_avg") %>% 
    pull(p.value)
}




# Test depth --------------------------------------------------------------
stats_data <- peaklist_pos_long %>%
                
                mutate(depth = as.factor(depth)) %>%
                
                filter(sample_type=="sample") %>% 
                filter(depth != "0,15 from bottom") %>%
                filter(!(depth == "2.5" & sample_point == "bottom")) %>% 
                filter(map_lgl(series_l,~ ..1 %in% c(1) %>% any)) %>%
    
                select(feature_id, pcgroup, Time, Tank, sample_point, value_drift_corr) %>% 
                nest(-Tank, -pcgroup, -feature_id)



cl <-  create_cluster( detectCores() )
plan(cluster, workers = cl)

stats_depth <- stats_data %>% 
                    mutate(p.value = future_map_dbl(data, stats_fun_depth))

stats_depth %<>% 
    select(-data) %>% 
    mutate(lfdr = fdrtool(p.value,statistic = "pvalue", plot=FALSE, verbose=FALSE)$lfdr)

# how many are significant
sum(stats_depth$lfdr<0.05)


# Test time ---------------------------------------------------------------
stats_data <- peaklist_pos_long %>%# filter(feature_id == 1000) %>% 
                    filter(map_lgl(series_l,~ ..1 %in% c(1,2,4) %>% any))


time_clust <- stats_data %>% 
               distinct(Time) %>% 
               mutate(time_clust = cutree(hclust(dist(Time)), h=10000)) %>% 
               select(Time, time_clust)


stats_data <- stats_data %>% 
                    left_join(time_clust, by="Time") %>% 
                    mutate(depth = as.factor(depth)) %>%            
                    
                    filter(depth != "0,15 from bottom") %>%
                    
                    group_by(time_clust, Tank, feature_id) %>%
                    mutate(n=n(), Time_avg = mean(Time)) %>%
                    ungroup %>% 
                    filter(n>1) %>% 
                    mutate(Time_avg = as.factor(Time_avg), sample_point = droplevels(sample_point)) %>%
                    select(Time, Time_avg, Tank, sample_point, n, time_clust, feature_id, pcgroup, value_drift_corr) %>% 
                    nest(-Tank, -pcgroup, -feature_id)
             


stats_time <- stats_data %>% 
                    mutate(p.value = future_map_dbl(data, stats_fun_time))

stats_time %<>% 
    select(-data) %>% 
    mutate(lfdr = fdrtool(p.value,statistic = "pvalue", plot=FALSE, verbose=FALSE)$lfdr)

# how many are significant
sum(stats_time$lfdr<0.05)

stats_time %>% filter(lfdr<0.05) %>% pull(pcgroup) %>% unique %>% length
    




# Test normalized depth --------------------------------------------------
stats_data <- peaklist_pos_long %>%
                
                mutate(depth = as.factor(depth)) %>%
                
                filter(sample_type=="sample") %>% 
                filter(depth != "0,15 from bottom") %>%
                filter(!(depth == "2.5" & sample_point == "bottom")) %>% 
                filter(map_lgl(series_l,~ ..1 %in% c(1) %>% any))
    
    
time_clust <- stats_data %>% 
               distinct(Time) %>% 
               mutate(time_clust = cutree(hclust(dist(Time)), h=10000)) %>% 
               select(Time, time_clust)


stats_data <-   stats_data %>% 
                left_join(time_clust, by="Time") %>% 
                select(feature_id, pcgroup, Time, Tank, sample_point, value_drift_corr, time_clust) %>% 
                nest(-Tank, -pcgroup, -feature_id)



stats_depth_norm <- stats_data %>% 
                    mutate(p.value = future_map_dbl(data, stats_fun_depth_norm))

stats_depth_norm %<>% 
    select(-data) %>% 
    mutate(lfdr = fdrtool(p.value,statistic = "pvalue", plot=FALSE, verbose=FALSE)$lfdr)

# how many are significant
sum(stats_depth_norm$lfdr<0.05)




# Close cluster    
stopCluster(cl)
rm(cl)



